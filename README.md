# Yobi (YOBI) #
--------------------

Copyright � 2017 Yobi

*"Just another waiting room."*

Yobi Dev, 2017

-----

### Yobi Wallet Download ###

-----

[Yobi-qt Bitbucket](https://bitbucket.org/yobicoin/yobi-qt-windows/downloads/)

-----

### Yobi Specifications ###

-----

>- **Cloned from YoviCoin, affectionately known as Yovi or YobitCoin**
>- Yobi [YOBI]
>- Sha256d
>- Hybrid till 300,000 blocks
>- 33.33 coins reward
>- 2 minute blocks
>- POS: 8%
>- minimum staking age, 3 minutes
>- maximum staking age, unlimited
>- Stake modifier: 30 seconds
>- Port: 6372
>- RPC Port: 6371
>- Premine: 20,000,000

>- Giveaways: YES, many coins

-----

This is a clone of YoviCoin, and is nearly a true reflection. 

### Differences: ###

>- Not a 100% premine
>- 300,000 hybrid blocks
>- Mining rewards
>- Normal starting difficulty
>- POS is up to 8% from 5%
>- Changed the wallet a bit


The rest is the same, apart from the usual shit you have to change to clone a coin.

-----

### Wallet bakground (bkg) ###

-----


![bkg.jpg](https://bitbucket.org/repo/7E9964b/images/2389580897-bkg.jpg)

-----

### Splash ###

-----

![splash3.png](https://bitbucket.org/repo/7E9964b/images/1489543486-splash3.png)


-----
